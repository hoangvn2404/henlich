class Api::StaffsController < ApplicationController

  def show
    # blocks = []
    # (0..40).each { |i| blocks << start_block + i*15.minutes }
    staff = Staff.find(params[:id])
    booked_blocks = staff.bookings.where("start_time::date = '#{params[:date].split("/").reverse().join("-")}'")
    start_block = Time.zone.parse("#{params[:date]}/#{Time.now.year} 08:00")
    blocks = (0..40).inject([]) { |acc, i| acc << (start_block + 15*i.minutes) }
    blocks = blocks.map do |block|
      {
        time: block.strftime('%H:%M'),
        available: filter_block(block, params[:duration].to_i, booked_blocks)
      }
    end
    render json: {blocks: blocks}
  end

  private

  def filter_block(block, duration, booked_blocks)
    block_start = block
    block_end = block + duration.minutes

    booked_blocks.all? do |booked_block|
      booked_block_start = booked_block.start_time
      booked_block_end = booked_block_start + booked_block.duration.minutes
      (block_end <= booked_block_start || block_start >= booked_block_end) && block_start != booked_block_start
    end
  end

  def staff_params
    params.require(:staff).permit!
  end
end
