export default () => {
  $('.daterange').daterangepicker({
      locale: { format: 'DD/MM/YYYY' },
      singleDatePicker: true,
      showDropdowns: true,
      autoUpdateInput: false
    })
  $('.daterange').on('apply.daterangepicker', function(ev, picker) { $(this).val(picker.startDate.format('DD/MM/YYYY')) })
  $('.daterange').on('cancel.daterangepicker', function(ev, picker) { $(this).val('') })
}
