import React, {useState, useEffect} from 'react'
import Staff from './staff'
import Location from './location'
import Service from './service'
import {CurrencyFormatted} from './../utilities'


const Booking = ({current_user, locations, staffs, services, start_time, end_time, block_time, today, tomorrow, next_tomorrow}) => {
  const [location, setLocation] = useState({})
  const [staff, setStaff] = useState({})
  const [selected_services, setSelectedServices] = useState([])
  const [date, setDate] = useState(today)
  const [time, setTime] = useState('')
  const [availableBlocks, setAvailableBlocks] = useState([])
  const [submitable, setSubmitable] = useState(false)
  const [customer, setCustomer] = useState({})
  // const [customerNumber, setCustomerNumber] = useState('')
  // const [customerName, setCustomerName] = useState('')

  const toggleService = s => {
    const new_selected_services = selected_services.includes(s) ? selected_services.filter(sv => sv !== s) : selected_services.concat(s)
    setSelectedServices(new_selected_services)
    if (new_selected_services.length === 0) {
      setTime('')
      setAvailableBlocks([])
    }
  }
  const sendRequest = () => {
    const booking = {
      location_id: location.id,
      staff_id: staff.id,
      service_ids: selected_services.map(s => s.id),
      start_time: `${date} ${time}`
    }

    $.post(`/api/bookings`, {booking, customer})
      .done(res => alert('Bạn đã đặt lịch thành công!'))
      .fail(res => console.log(res))
  }

  const getStaffAvailableTimes = staff =>
    $.get(`/api/staffs/${staff.id}`, {date: date, duration: selected_services.map(s => s.duration).reduce((x, y) => x + y, 0)})
      .done(res => setAvailableBlocks(res.blocks))


  useEffect(() => {
    (!!location.id && !!staff.id && selected_services != []) && getStaffAvailableTimes(staff)
    setSubmitable((!!location.id && !!staff.id && !!time && selected_services != [] && !!customer.phone && !!customer.name))

  }, [date, staff, selected_services, time, customer]);

  useEffect(() => {

  })

  return (
    <div className='row'>
      <div className='col-8'>
        <h2>Please select your booking</h2>
        <table className='table table-sm' style={{tableLayout: 'fixed'}}>
          <tbody>
            <tr>
              <td className='border-top-0'>Chọn Địa điểm</td>
              <td className='border-top-0' colSpan='3'>
                {locations.map(s => <Location key={s.id} location={s} setLocation={() => setLocation(s)} active={s.id === location.id}/>)}
              </td>
            </tr>

            <tr>
              <td>Chọn Dịch Vụ</td>
              <td colSpan='3'>
                <table className='table table-sm table-borderless'>
                  <tbody>
                    {services.map(s => <Service key={s.id} service={s} toggleService={() => toggleService(s)} active={selected_services.includes(s)} />)}
                  </tbody>
                </table>
              </td>
            </tr>

            <tr>
              <td>Chọn Nhân Viên</td>
              <td colSpan='3'>
                {staffs.filter(s => s.location_id === location.id).map(s => <Staff key={s.id} staff={s} setStaff={() => {setStaff(s); setTime('')}} active={s.id === staff.id}/>)}
              </td>
            </tr>

            <tr>
              <td>Chọn Thời Gian</td>
              <td colSpan='3'>
                <div className='container p-0'>
                  <button className={`btn mr-1 ${date === today ? 'btn-info' : 'btn-light'}`} onClick={() => setDate(today)}>Hôm nay</button>
                  <button className={`btn mr-1 ${date === tomorrow ? 'btn-info' : 'btn-light'}`} onClick={() => setDate(tomorrow)}>{tomorrow.slice(0,5)}</button>
                  <button className={`btn mr-1 ${date === next_tomorrow ? 'btn-info' : 'btn-light'}`} onClick={() => setDate(next_tomorrow)}>{next_tomorrow.slice(0,5)}</button>
                </div>
                <hr></hr>
                <div className='container mx-auto text-center'>
                {availableBlocks.map(block =>
                    <button key={block.time}
                            className={`avatar btn mb-2 mr-2 ${
                              block.available
                                ? block.time === time ? 'btn-success' : 'btn-outline-primary'
                                : 'btn-light disabled'
                              }`}
                            disabled={!block.available}
                            onClick={() => setTime(block.time)}
                            style={{width: '70px'}}>
                      {block.time}
                      <div className='overlay'>
                        <i className="fas fa-check"></i>
                      </div>
                    </button>
                )}
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div className='col-4'>
        <h2>Appointment Summary</h2>
        <table className='table table-bordered' style={{tableLayout: 'fixed'}}>
          <tbody>
            <tr>
              <td>
                <label>Địa điểm</label>
                {location && <p className="float-right">{location.name}</p>}
              </td>
            </tr>

            <tr>
              <td>
                <label>Dịch Vụ</label>

                <ul className="p-0" style={{listStyle: 'none'}}>
                  {selected_services.map(service =>
                    <li key={service.id} className='clearfix'>
                    <p className="mb-0">
                      <button className='btn btn-sm' onClick={() => toggleService(service)}>
                        <i className="far fa-minus-square"></i>
                      </button>
                      {service.name}
                     </p>
                    <small className="float-left"><i>{service.duration} phút</i></small>
                    <small className="float-right">{CurrencyFormatted(service.price)} VND</small>
                   </li>
                  )}
                </ul>
              </td>
            </tr>

            <tr>
              <td>
                <label>Nhân Viên</label>
                {staff && <p className="float-right">{staff.name}</p>}
              </td>
            </tr>
            <tr>
              <td>
                Tổng
                <p className='float-right'>{ CurrencyFormatted(selected_services.map(s => s.price).reduce((x, y) => x + y, 0)) } VND</p>
              </td>

            </tr>
            <tr>
              <td>
                Thời gian thực hiện
                <p className='float-right'>{selected_services.map(s => s.duration).reduce((x, y) => x + y, 0) } phút</p>
              </td>
            </tr>

            <tr>
              <td>
                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <div className="input-group-text"><i className="fas fa-phone-square"></i></div>
                  </div>
                  <input type="text" className="form-control" placeholder="Số điện thoại khách hàng" value={customer.phone} onChange={(e) => setCustomer({...customer, phone: e.target.value})}/>
                </div>

                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <div className="input-group-text"><i className="far fa-user"></i></div>
                  </div>
                  <input type="text" className="form-control" placeholder="Tên của khách hàng" value={customer.name} onChange={(e) => setCustomer({...customer, name: e.target.value})} />
                </div>
              </td>
            </tr>

            <tr>
              <td>
                <button className={`btn btn-block btn-success ${submitable ? '' : 'disabled'}`} onClick={sendRequest} disabled={!submitable}>
                  <i className="fas fa-stopwatch mr-1"></i>
                  Đặt lịch
                </button>
              </td>
            </tr>
          </tbody>
        </table>

      </div>
    </div>
  )
}

export default Booking
