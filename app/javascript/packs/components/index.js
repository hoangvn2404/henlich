import WebpackerReact from 'webpacker-react'
import Booking from './booking.js'
import FullCalendar from './full_calendar'
import Hello from './hello'

export default () => WebpackerReact.setup({
  Booking,
  FullCalendar,
  Hello,
})

// const requireModule = require.context('.', true, /\.js$/)
// const modules = {}
// const get_component_name = (fileName) => fileName.replace('./', '').replace('.js', '').split('_').map(s => s[0].toUpperCase() + s.slice(1)).join('')

// requireModule
//   .keys()
//   .filter(fileName => fileName !== './index.js')
//   .forEach(fileName => modules[get_component_name(fileName)] = requireModule(fileName).default)

// export default () => WebpackerReact.setup(modules)
