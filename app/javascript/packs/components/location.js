import React from 'react'

const Location = ({location, setLocation, active}) => {
  return (
    <div className="text-center d-inline-block p-1">
      <button className={`avatar btn rounded-circle shadow p-1 d-block ${active && 'active'}`} onClick={setLocation}>
        <img src='https://picsum.photos/65/65' className='rounded-circle' style={{width: '65px', height: '65px'}}/>
        <div className='overlay'>
          <i className="fas fa-check"></i>
        </div>
      </button>
      <label className='mt-1'>{location.name}</label>
    </div>
  )
}

export default Location
