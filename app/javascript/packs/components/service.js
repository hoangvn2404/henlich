import React, {useState} from 'react'
import {CurrencyFormatted} from './../utilities'

const Service = ({service, toggleService, active}) => {
  return (
    <tr>
      <td>
        <p className='mb-0'>{service.name}</p>
        <small><i>{service.duration} phút</i></small>
      </td>
      <td className='text-right'>
        <p className='mb-0'>{CurrencyFormatted(service.price)} VND</p>
      </td>
      <td width='60px'>
        <button className='btn btn-sm' onClick={toggleService}>
          {active
            ? <i className="far fa-minus-square fa-2x"></i>
            : <i className="far fa-plus-square fa-2x text-info"></i>
          }
        </button>
      </td>
    </tr>
  )
}

export default Service
