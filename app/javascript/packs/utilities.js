const CurrencyFormatted = amount => {
  amount = amount.toString(10).split('').reverse()
  let text = ''
  for (var i = 0; i < amount.length; i++) {
   text = (i+1)%3 === 0 && i+1 !== amount.length
     ? text.concat(amount[i]).concat(',')
     : text.concat(amount[i])
  }
  return text.split('').reverse().join('')
}

export {CurrencyFormatted}
