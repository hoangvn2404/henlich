# == Schema Information
#
# Table name: bookings
#
#  id          :bigint           not null, primary key
#  customer_id :integer
#  location_id :integer
#  staff_id    :integer
#  status      :string           default("pending")
#  date        :date
#  time        :time
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Booking < ApplicationRecord
  belongs_to :customer
  belongs_to :staff
  belongs_to :location
  has_many :booking_service_connects
  has_many :services, through: :booking_service_connects
  accepts_nested_attributes_for :services

  enum status: { pending: 'pending', completed: 'completed', canceled: 'canceled' }

  def duration
    services.map { |s| s.duration }.sum
  end
end
