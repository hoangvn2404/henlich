# == Schema Information
#
# Table name: public.clients
#
#  id         :bigint           not null, primary key
#  name       :string
#  subdomain  :string
#  industry   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Client < ApplicationRecord
  after_create :create_schema

  def create_schema
    if subdomain && !Client.connection.schema_exists?(subdomain)
      ActiveRecord::Base.uncached do
        Apartment::Tenant.create(subdomain)
      end
    end
  end

  def switch!
    Apartment::Tenant.switch! subdomain
  end
end
