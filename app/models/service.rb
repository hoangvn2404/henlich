# == Schema Information
#
# Table name: services
#
#  id         :bigint           not null, primary key
#  name       :string
#  price      :integer
#  detail     :text
#  duration   :integer
#  category   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Service < ApplicationRecord
  has_many :booking_service_connects
  has_many :bookings, through: :booking_service_connects
end
