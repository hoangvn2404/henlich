# == Schema Information
#
# Table name: staffs
#
#  id            :bigint           not null, primary key
#  name          :string
#  date_of_birth :date
#  gender        :string
#  phone         :string
#  location_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Staff < ApplicationRecord
  belongs_to :location
  has_many :bookings
  validates_presence_of :name, :phone, :gender, :location_id
end
