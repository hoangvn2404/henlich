class CreateStaffs < ActiveRecord::Migration[5.2]
  def change
    create_table :staffs do |t|
      t.string :name
      t.date :date_of_birth
      t.string :gender
      t.string :phone, index: true
      t.integer :location_id, index: true

      t.timestamps
    end
  end
end
