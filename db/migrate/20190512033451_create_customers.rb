class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :gender
      t.date :date_of_birth
      t.string :phone, index: true, unique: true
      t.string :facebook

      t.timestamps
    end
  end
end
