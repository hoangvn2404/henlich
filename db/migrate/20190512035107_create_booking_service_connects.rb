class CreateBookingServiceConnects < ActiveRecord::Migration[5.2]
  def change
    create_table :booking_service_connects do |t|
      t.integer :booking_id, index: true
      t.integer :service_id, index: true

      t.timestamps
    end

    add_index :booking_service_connects, [:booking_id, :service_id], unique: true
  end
end
